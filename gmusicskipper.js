// This script will monitor the Google Music page and skip songs you've already downvoted.
// Google doesn't do this automatically for some weird reason, so this hacks around that.
// Note that you're limited to somewhere around five skips per hour per station, so you
// can pretty quickly run into a scenario where this won't work. In the case that you want
// to stop the script, just run clearInterval(skipper) after this script.

var skipper = setInterval(function() {
    if(document.querySelectorAll('tr.currently-playing > td[data-rating="1"]').length > 0) {
        document.getElementById('player-bar-forward').click(); }
}, 5000);

